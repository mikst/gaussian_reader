from __future__ import print_function
from gaussian_read import Reader
import glob

fnames = glob.glob('*.log')
fnames += (glob.glob('*.out'))
fnames.sort()
fout = open('out.txt', 'w')  # text output file

for fname in fnames:
    print(fname)
    try:
        r = Reader(fname)
    except:
        print('skipping file: ' + fname)
        continue
    line = ''
    energy = r.get_energy()
    eh, el = r.get_homo_lumo_levels()
    egap = el - eh
    line = '{0} '.format(energy)
    for x in [eh, el, egap]:
        for s in range(len(eh)):
            line += '{0} '.format(x[s])

    line += fname  # also write the filename
    fout.write(line + '\n')

