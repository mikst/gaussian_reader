from __future__ import print_function
import numpy as np
from ase.units import Hartree


def extract_eigs_occs(lines):
    eigs = []
    occs = []
    for line in lines:
        values = line.split()
        eigs.append(float(values[2]))
        occs.append(['V', 'O'].index(values[1]))

    eigs = np.asarray(eigs, float)
    occs = np.asarray(occs, float)
    return eigs, occs


class Reader:
    def __init__(self, fname):
        with open(fname, 'r') as f:
            self.line = f.read()

        self.data = {}
        self.conv = {'HF': 'energy'}
        self.read_energy()
        self.read_eigenvalues_and_occupations()

    def read_energy(self):
        line = self.line
        i1 = line.rfind('Total kinetic energy from orbitals')
        i2 = line[i1:].find('\\@')
        lines = line[i1: i1 + i2].splitlines()
        data = ''.join([l.strip() for l in lines])
        i1 = data.find('HF=')
        i2 = data[i1:].find('\\')
        e = float(data[i1: i1 + i2].split('=')[1])
        self.data['energy'] = e * Hartree

    def read_eigenvalues_and_occupations(self):
        l = self.line
        x = l.rfind('Orbital energies and kinetic energies (beta)')
        if x != -1:
            i1 = l.rfind('Orbital energies and kinetic energies (alpha)')
            i2 = i1 + l[i1:].rfind(
                'Orbital energies and kinetic energies (beta)')
            lines = l[i1: i2].splitlines()[2:-1]
            eigs1, occs1 = extract_eigs_occs(lines)
            i1 = l.rfind('Orbital energies and kinetic energies (beta)')
            i2 = i1 + l[i1:].find('Total kinetic energy from orbitals')
            lines = l[i1: i2].splitlines()[2:-1]
            eigs2, occs2 = extract_eigs_occs(lines)
            self.data['eigenvalues'] = np.vstack((eigs1, eigs2)) * Hartree
            self.data['occupations'] = np.vstack([occs1, occs2])
        else:
            i1 = l.rfind('Orbital energies and kinetic energies (alpha)')
            i2 = i1 + self.line[i1:].find('Total kinetic energy from orbitals')
            lines = self.line[i1: i2].splitlines()[2:-1]
            eigs, occs = extract_eigs_occs(lines)
            self.data['eigenvalues'] = eigs.reshape(1, -1) * Hartree
            self.data['occupations'] = occs.reshape(1, -1)

    def get_homo_lumo_levels(self):
        e_sn = self.data['eigenvalues']
        f_sn = self.data['occupations']
        ns = e_sn.shape[0]
        homos = []
        lumos = []
        for s in range(ns):
            eigs, occs = e_sn[s], f_sn[s]
            i_homo = int(occs.sum()) - 1
            homos.append(eigs[i_homo])
            lumos.append(eigs[i_homo + 1])

        return np.asarray(homos), np.asarray(lumos)

    def get_energy(self):
        return self.data['energy']

    def __getattr__(self, name):
        if name in self.conv:
            return self.data.get(self.conv[name])
        else:
            return self.data.get('name')


if __name__ == '__main__':
    # example of writing stuff to a text file.
    # 
    #import glob
    #fnames = glob.glob('*.log')  # find all files ending with .log
    #fnames.sort()
    fnames = ['1.log', '2.log']  # gaussian files
    fout = open('out.txt', 'w')  # text output file 
    fout.write('# energy eh el egap input_file\n')
    for fname in fnames:
        r = Reader(fname)
        line = ''
        energy = r.get_energy()
        eh, el = r.get_homo_lumo_levels()
        egap = el - eh
        line = '{0} '.format(energy)
        for x in [eh, el, egap]:
            for s in range(len(eh)):
                line += '{0} '.format(x[s])

        line += fname  # also write the filename
        fout.write(line + '\n')

